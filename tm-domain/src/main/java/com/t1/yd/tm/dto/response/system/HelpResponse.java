package com.t1.yd.tm.dto.response.system;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public class HelpResponse extends AbstractResultResponse {

    private List<String> helpStrings;

    public HelpResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public HelpResponse() {
    }

}

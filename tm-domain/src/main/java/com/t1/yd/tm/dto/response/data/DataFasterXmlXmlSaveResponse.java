package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlXmlSaveResponse extends AbstractResultResponse {

    public DataFasterXmlXmlSaveResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataFasterXmlXmlSaveResponse() {

    }
}

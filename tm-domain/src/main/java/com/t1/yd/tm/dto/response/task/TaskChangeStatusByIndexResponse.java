package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskChangeStatusByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}

package com.t1.yd.tm.dto.request.user;

import com.t1.yd.tm.dto.request.AbstractRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class UserRegistryRequest extends AbstractRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private String email;
}

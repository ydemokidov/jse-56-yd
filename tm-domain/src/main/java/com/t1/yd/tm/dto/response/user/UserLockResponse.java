package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserLockResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

}

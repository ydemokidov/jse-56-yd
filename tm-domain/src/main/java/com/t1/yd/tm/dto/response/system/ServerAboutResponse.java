package com.t1.yd.tm.dto.response.system;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ServerAboutResponse extends AbstractResultResponse {

    private String email;

    private String name;

    public ServerAboutResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public ServerAboutResponse() {
    }

}

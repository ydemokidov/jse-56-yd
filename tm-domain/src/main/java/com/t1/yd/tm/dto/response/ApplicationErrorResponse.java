package com.t1.yd.tm.dto.response;

import org.jetbrains.annotations.Nullable;

public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}

package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataBase64SaveResponse extends AbstractResultResponse {

    public DataBase64SaveResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataBase64SaveResponse() {

    }
}

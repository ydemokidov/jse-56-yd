package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@Nullable final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskShowByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}

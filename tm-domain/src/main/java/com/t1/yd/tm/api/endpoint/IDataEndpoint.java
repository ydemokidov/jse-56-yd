package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.response.data.*;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDataEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DataEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IDataEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IDataEndpoint newInstance(@NotNull IConnectionProvider connectionProvider) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDataEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IDataEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDataEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse backupLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBackupLoadRequest request);

    @NotNull
    @WebMethod
    DataBackupSaveResponse backupSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBackupSaveRequest request);

    @NotNull
    @WebMethod
    DataBinaryLoadResponse binaryLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBinaryLoadRequest request);

    @NotNull
    @WebMethod
    DataBinarySaveResponse binarySave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBinarySaveRequest request);

    @NotNull
    @WebMethod
    DataBase64LoadResponse base64Load(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64LoadRequest request);

    @NotNull
    @WebMethod
    DataBase64SaveResponse base64Save(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64SaveRequest request);

    @NotNull
    @WebMethod
    DataFasterXmlJsonSaveResponse fasterXmlJsonSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataFasterXmlJsonSaveRequest request);

    @NotNull
    @WebMethod
    DataFasterXmlJsonLoadResponse fasterXmlJsonLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataFasterXmlJsonLoadRequest request);

    @NotNull
    @WebMethod
    DataFasterXmlXmlSaveResponse fasterXmlXmlSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataFasterXmlXmlSaveRequest request);

    @NotNull
    @WebMethod
    DataFasterXmlXmlLoadResponse fasterXmlXmlLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataFasterXmlXmlLoadRequest request);

    @NotNull
    @WebMethod
    DataFasterXmlYmlSaveResponse fasterXmlYmlSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataFasterXmlYmlSaveRequest request);

    @NotNull
    @WebMethod
    DataFasterXmlYmlLoadResponse fasterXmlYmlLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataFasterXmlYmlLoadRequest request);

    @NotNull
    @WebMethod
    DataJaxbJsonLoadResponse jaxbJsonLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJaxbJsonLoadRequest request);

    @NotNull
    @WebMethod
    DataJaxbJsonSaveResponse jaxbJsonSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJaxbJsonSaveRequest request);

    @NotNull
    @WebMethod
    DataJaxbXmlLoadResponse jaxbXmlLoad(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJaxbXmlLoadRequest request);

    @NotNull
    @WebMethod
    DataJaxbXmlSaveResponse jaxbXmlSave(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJaxbXmlSaveRequest request);

}

package com.t1.yd.tm.command.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataJaxbXmlLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataJaxbXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "load_jaxb_xml";
    @NotNull
    private final String description = "Load data from XML with JAXB library";

    @Autowired
    public DataJaxbXmlLoadCommand(@NotNull final ITokenService tokenService,
                                  @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[JAXB XML DATA LOAD]");
        @NotNull final DataJaxbXmlLoadRequest request = new DataJaxbXmlLoadRequest();
        getDataEndpointClient().jaxbXmlLoad(request);
        System.out.println("[DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}

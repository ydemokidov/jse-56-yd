package com.t1.yd.tm.command.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.task.TaskCompleteByIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_complete_by_id";
    @NotNull
    public static final String DESCRIPTION = "Complete task by Id";

    @Autowired
    public TaskCompleteByIdCommand(@NotNull final ITokenService tokenService,
                                   @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setId(id);
        request.setToken(getToken());
        getTaskEndpointClient().completeTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";
    @NotNull
    public static final String ARGUMENT = "-v";
    @NotNull
    public static final String DESCRIPTION = "Show program version";

    @Autowired
    public ApplicationVersionCommand(@NotNull final ITokenService tokenService,
                                     @NotNull final ICommandService commandService,
                                     @NotNull final IPropertyService propertyService) {
        super(tokenService, commandService, propertyService);
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}

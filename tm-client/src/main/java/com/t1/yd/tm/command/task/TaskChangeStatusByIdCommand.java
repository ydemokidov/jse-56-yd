package com.t1.yd.tm.command.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.task.TaskChangeStatusByIdRequest;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;

@Component
public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_change_status_by_id";
    @NotNull
    public static final String DESCRIPTION = "Change task status by Id";

    @Autowired
    public TaskChangeStatusByIdCommand(@NotNull final ITokenService tokenService,
                                       @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Objects.requireNonNull(Status.toStatus(statusValue));
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setId(id);
        request.setStatus(status.toString());
        request.setToken(getToken());
        getTaskEndpointClient().changeTaskStatusById(request);

    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

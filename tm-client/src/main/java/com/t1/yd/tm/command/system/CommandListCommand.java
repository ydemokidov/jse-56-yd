package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";
    @NotNull
    public static final String ARGUMENT = "-comm";
    @NotNull
    public static final String DESCRIPTION = "Show available commands";

    @Autowired
    public CommandListCommand(@NotNull final ITokenService tokenService,
                              @NotNull final ICommandService commandService,
                              @NotNull final IPropertyService propertyService) {
        super(tokenService, commandService, propertyService);
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");

        for (@NotNull final String commandName : getCommandNamesList()) {
            System.out.println(commandName);
        }

    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    public List<String> getCommandNamesList() {
        List<String> commandNames = new ArrayList<>();

        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();

        for (@NotNull final ICommand command : commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            commandNames.add(command.getName());
        }

        return commandNames;
    }

}

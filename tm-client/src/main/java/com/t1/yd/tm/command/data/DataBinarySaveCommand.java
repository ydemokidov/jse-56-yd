package com.t1.yd.tm.command.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataBinarySaveRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_binary";
    @NotNull
    private final String description = "Save data to binary file";

    @Autowired
    public DataBinarySaveCommand(@NotNull final ITokenService tokenService,
                                 @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BINARY SAVE DATA]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        request.setToken(getToken());
        getDataEndpointClient().binarySave(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}

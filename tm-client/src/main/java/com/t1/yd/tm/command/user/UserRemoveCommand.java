package com.t1.yd.tm.command.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.UserRemoveRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_remove";
    @NotNull
    private final String description = "Remove user";

    @Autowired
    public UserRemoveCommand(@NotNull final ITokenService tokenService,
                             @NotNull final IUserEndpoint userEndpointClient,
                             @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull UserRemoveRequest request = new UserRemoveRequest();
        request.setLogin(login);
        request.setToken(getToken());

        @NotNull final UserDTO removedUserDTO = getUserEndpoint().remove(request).getUserDTO();

        System.out.println("[USER " + removedUserDTO.getLogin() + " REMOVED]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}

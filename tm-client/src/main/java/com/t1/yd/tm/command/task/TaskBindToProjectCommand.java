package com.t1.yd.tm.command.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.task.TaskBindToProjectRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_bind_to_project";
    @NotNull
    public static final String DESCRIPTION = "Bind task to project";

    @Autowired
    public TaskBindToProjectCommand(@NotNull final ITokenService tokenService,
                                    @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest();
        request.setId(taskId);
        request.setProjectId(projectId);
        request.setToken(getToken());
        getTaskEndpointClient().bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

package com.t1.yd.tm.command.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    private final IDataEndpoint dataEndpointClient;

    public AbstractDataCommand(@NotNull final ITokenService tokenService,
                               @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService);
        this.dataEndpointClient = dataEndpointClient;
    }

    @NotNull
    protected IDataEndpoint getDataEndpointClient() {
        return dataEndpointClient;
    }

}

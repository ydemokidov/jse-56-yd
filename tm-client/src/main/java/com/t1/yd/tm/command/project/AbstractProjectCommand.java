package com.t1.yd.tm.command.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected final IProjectEndpoint projectEndpointClient;

    public AbstractProjectCommand(@NotNull final ITokenService tokenService,
                                  @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService);
        this.projectEndpointClient = projectEndpointClient;
    }

    @NotNull
    protected IProjectEndpoint getProjectEndpointClient() {
        return projectEndpointClient;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@NotNull final ProjectDTO projectDTO) {
        System.out.println("ID: " + projectDTO.getId());
        System.out.println("NAME: " + projectDTO.getName());
        System.out.println("DESC: " + projectDTO.getDescription());
        System.out.println("STATUS: " + Status.toName(projectDTO.getStatus()));
    }

}

package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "help";
    @NotNull
    public static final String ARGUMENT = "-h";
    @NotNull
    public static final String DESCRIPTION = "Show info about program";

    @Autowired
    public ApplicationHelpCommand(@NotNull final ITokenService tokenService,
                                  @NotNull final ICommandService commandService,
                                  @NotNull final IPropertyService propertyService) {
        super(tokenService, commandService, propertyService);
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (String commandHelp : getHelpStrings()) {
            System.out.println(commandHelp);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    public List<String> getHelpStrings() {
        List<String> helpStrings = new ArrayList<>();
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) helpStrings.add(command.toString());
        return helpStrings;
    }
}

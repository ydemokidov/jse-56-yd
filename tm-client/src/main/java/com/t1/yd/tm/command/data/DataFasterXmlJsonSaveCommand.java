package com.t1.yd.tm.command.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataFasterXmlJsonSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataFasterXmlJsonSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_fasterxml_json";
    @NotNull
    private final String description = "Save json with FasterXML library";

    @Autowired
    public DataFasterXmlJsonSaveCommand(@NotNull final ITokenService tokenService,
                                        @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[DATA SAVE FASTERXML]");
        @NotNull final DataFasterXmlJsonSaveRequest request = new DataFasterXmlJsonSaveRequest();
        request.setToken(getToken());
        getDataEndpointClient().fasterXmlJsonSave(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}

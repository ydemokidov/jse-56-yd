package com.t1.yd.tm.command.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.user.UserPasswordChangeRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserPasswordChangeCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_password_change";
    @NotNull
    private final String description = "Change user password";

    @Autowired
    public UserPasswordChangeCommand(@NotNull final ITokenService tokenService,
                                     @NotNull final IUserEndpoint userEndpointClient,
                                     @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPwd = TerminalUtil.nextLine();
        @NotNull final UserPasswordChangeRequest request = new UserPasswordChangeRequest();
        request.setNewPassword(newPwd);
        request.setToken(getToken());
        getUserEndpoint().passwordChange(request);
        System.out.println("[PASSWORD CHANGED]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}

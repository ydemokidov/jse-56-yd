package com.t1.yd.tm.command.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.project.ProjectClearRequest;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_clear";
    @NotNull
    public static final String DESCRIPTION = "Clear project";

    @Autowired
    public ProjectClearCommand(@NotNull final ITokenService tokenService,
                               @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService, projectEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        request.setToken(getToken());
        getProjectEndpointClient().clearProjects(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }
}

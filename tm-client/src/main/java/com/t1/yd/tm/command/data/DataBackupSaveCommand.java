package com.t1.yd.tm.command.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataBackupSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "save_backup";
    @NotNull
    public final String description = "Save backup to file";

    @Autowired
    public DataBackupSaveCommand(@NotNull final ITokenService tokenService,
                                 @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest();
        request.setToken(getToken());
        getDataEndpointClient().backupSave(request);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}

package com.t1.yd.tm.command.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.project.ProjectRemoveByIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {
    @NotNull
    public static final String NAME = "project_remove_by_id";
    @NotNull
    public static final String DESCRIPTION = "Remove project by Id";

    @Autowired
    public ProjectRemoveByIdCommand(@NotNull final ITokenService tokenService,
                                    @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService, projectEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest();
        request.setId(id);
        request.setToken(getToken());
        getProjectEndpointClient().removeProjectById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}

package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "exit";
    @Nullable
    public static final String ARGUMENT = null;
    @NotNull
    public static final String DESCRIPTION = "Exit program";

    @Autowired
    public ApplicationExitCommand(@NotNull final ITokenService tokenService,
                                  @NotNull final ICommandService commandService,
                                  @NotNull final IPropertyService propertyService) {
        super(tokenService, commandService, propertyService);
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

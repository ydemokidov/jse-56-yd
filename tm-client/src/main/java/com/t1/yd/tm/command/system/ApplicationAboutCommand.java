package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";
    @NotNull
    public static final String ARGUMENT = "-a";
    @NotNull
    public static final String DESCRIPTION = "Show info about author";

    @Autowired
    public ApplicationAboutCommand(@NotNull final ITokenService tokenService,
                                   @NotNull final ICommandService commandService,
                                   @NotNull final IPropertyService propertyService) {
        super(tokenService, commandService, propertyService);
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Author: " + getPropertyService().getAuthorName());
        System.out.println("Email: " + getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

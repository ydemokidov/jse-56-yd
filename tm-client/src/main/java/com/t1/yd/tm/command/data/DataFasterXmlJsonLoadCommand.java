package com.t1.yd.tm.command.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataFasterXmlJsonLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataFasterXmlJsonLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "load_fasterxml_json";
    @NotNull
    private final String description = "Load JSON with FasterXml library";

    @Autowired
    public DataFasterXmlJsonLoadCommand(@NotNull final ITokenService tokenService,
                                        @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    public void execute() {
        System.out.println("[DATA LOAD FASTERXML JSON]");
        @NotNull final DataFasterXmlJsonLoadRequest request = new DataFasterXmlJsonLoadRequest();
        request.setToken(getToken());
        getDataEndpointClient().fasterXmlJsonLoad(request);
        System.out.println("[DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}

package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    private final ICommandService commandService;

    @NotNull
    private final IPropertyService propertyService;

    public AbstractSystemCommand(@NotNull final ITokenService tokenService,
                                 @NotNull final ICommandService commandService,
                                 @NotNull final IPropertyService propertyService) {
        super(tokenService);
        this.commandService = commandService;
        this.propertyService = propertyService;
    }

    @NotNull
    protected ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}

package com.t1.yd.tm.command.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_logout";
    @NotNull
    private final String description = "User logout";

    @Autowired
    public UserLogoutCommand(@NotNull final ITokenService tokenService,
                             @NotNull final IUserEndpoint userEndpointClient,
                             @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(getToken());
        getAuthEndpoint().logout(request);
        System.out.println("[USER LOGGED OUT]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}

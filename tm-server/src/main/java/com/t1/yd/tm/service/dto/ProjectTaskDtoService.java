package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.service.dto.IDtoProjectTaskService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public final class ProjectTaskDtoService implements IDtoProjectTaskService {

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final ITaskDtoService taskService;

    @Autowired
    public ProjectTaskDtoService(@NotNull final IProjectDtoService projectService,
                                 @NotNull final ITaskDtoService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        if (projectService.findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        @Nullable final TaskDTO taskDTO = taskService.findOneById(userId, taskId);
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setProjectId(projectId);
        taskService.update(taskDTO);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();

        if (projectService.findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        @NotNull final List<TaskDTO> taskDTOS = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final TaskDTO taskDTO : taskDTOS) {
            taskService.removeById(taskDTO.getId());
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();

        @Nullable final TaskDTO taskDTO = taskService.findOneById(userId, taskId);
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setProjectId(null);
        taskService.update(taskDTO);
    }

}

package com.t1.yd.tm.api.repository.dto;

import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IDtoTaskRepository extends IDtoUserOwnedRepository<TaskDTO> {

    @NotNull List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull List<TaskDTO> findAll(@NotNull String sort);

}

package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoUserRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Objects;

@Service
public final class UserDtoService extends AbstractDtoService<UserDTO, IDtoUserRepository> implements IUserDtoService {

    @NotNull
    private final ISaltProvider saltProvider;

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final ITaskDtoService taskService;

    @Autowired
    public UserDtoService(@NotNull final ISaltProvider saltProvider,
                          @NotNull final ILoggerService loggerService,
                          @NotNull final ApplicationContext context,
                          @NotNull final IProjectDtoService projectService,
                          @NotNull final ITaskDtoService taskService) {
        super(loggerService, context);
        this.saltProvider = saltProvider;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    @Override
    protected IDtoUserRepository getRepository() {
        return context.getBean(IDtoUserRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();

        remove(userDTO);
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeByEmail(@NotNull final String email) {
        @Nullable final UserDTO userDTO = findByEmail(email);
        if (userDTO == null) throw new UserNotFoundException();

        remove(userDTO);
        return userDTO;
    }

    @NotNull
    @SneakyThrows
    public UserDTO remove(@NotNull final UserDTO userDTOToRemove) {
        @NotNull final IDtoUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final UserDTO userDTO = findOneById(userDTOToRemove.getId());
            if (userDTO == null) throw new UserNotFoundException();

            taskService.clear(userDTOToRemove.getId());

            projectService.clear(userDTOToRemove.getId());

            entityManager.getTransaction().begin();

            repository.remove(userDTOToRemove);

            entityManager.getTransaction().commit();
            return userDTO;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final UserDTO userDTO = findOneById(id);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        update(userDTO);
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(@NotNull final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final UserDTO userDTO = findOneById(id);
        if (userDTO == null) throw new UserNotFoundException();

        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setMiddleName(middleName);

        update(userDTO);
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        return add(userDTO);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new IsEmailExistException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        userDTO.setEmail(email);
        return add(userDTO);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @Nullable Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        userDTO.setEmail(email);
        userDTO.setRole(role);
        return add(userDTO);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO userDTO;
        @NotNull final IDtoUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            userDTO = repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO userDTO;
        @NotNull final IDtoUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            userDTO = repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull final String login) {
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setLocked(true);
        update(userDTO);
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull final String login) {
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setLocked(false);
        update(userDTO);
    }

}
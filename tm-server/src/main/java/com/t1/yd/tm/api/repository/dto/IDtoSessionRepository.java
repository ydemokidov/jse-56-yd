package com.t1.yd.tm.api.repository.dto;

import com.t1.yd.tm.dto.model.SessionDTO;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IDtoSessionRepository extends IDtoUserOwnedRepository<SessionDTO> {

    @NotNull List<SessionDTO> findAll(@NotNull String sort);

}

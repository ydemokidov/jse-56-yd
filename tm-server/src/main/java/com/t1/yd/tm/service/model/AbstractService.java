package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.IRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.IService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.model.AbstractEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected final ApplicationContext context;

    @NotNull
    protected final ILoggerService loggerService;

    public AbstractService(@NotNull final ILoggerService loggerService,
                           @NotNull final ApplicationContext context) {
        this.context = context;
        this.loggerService = loggerService;
    }

    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final E entity) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final Comparator comparator) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final E result = findOneById(id);
            if (result == null) throw new EntityNotFoundException();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final E result = findOneByIndex(index);
            if (result == null) throw new EntityNotFoundException();
            repository.removeByIndex(index);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll().size();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            collection.forEach(repository::add);
            entityManager.getTransaction().commit();
            return collection;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            collection.forEach(repository::add);
            entityManager.getTransaction().commit();
            return collection;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public E update(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final IRepository<E> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entity;
    }

}

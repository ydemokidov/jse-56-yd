package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoSessionRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.ISessionDtoService;
import com.t1.yd.tm.dto.model.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Service
public class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, IDtoSessionRepository> implements ISessionDtoService {

    @Autowired
    public SessionDtoService(@NotNull final ILoggerService loggerService,
                             @NotNull final ApplicationContext context) {
        super(loggerService, context);
    }

    @NotNull
    @Override
    protected IDtoSessionRepository getRepository() {
        return context.getBean(IDtoSessionRepository.class);
    }


    @Override
    public @NotNull SessionDTO update(@NotNull SessionDTO entity) {
        throw new NotImplementedException();
    }

}

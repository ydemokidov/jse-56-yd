package com.t1.yd.tm.component;

import com.t1.yd.tm.api.service.IDomainService;
import com.t1.yd.tm.service.DomainService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class Backup {

    @NotNull
    private final IDomainService domainService;

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @Autowired
    public Backup(@NotNull IDomainService domainService) {
        this.domainService = domainService;
    }

    @PostConstruct
    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        domainService.dataBackupSave();
    }

    public void load() {
        if (Files.exists(Paths.get(DomainService.FILE_BACKUP)))
            domainService.dataBackupLoad();
    }

}

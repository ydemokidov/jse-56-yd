package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.IProjectRepository;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Autowired
    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Project> getClazz() {
        return Project.class;
    }

    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }
}

package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.ISessionRepository;
import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Autowired
    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Session> getClazz() {
        return Session.class;
    }

}

package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.dto.IDtoProjectTaskService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.configuration.ServerConfiguration;
import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.t1.yd.tm.constant.ProjectTestData.USER_1_PROJECT_DTO_1;
import static com.t1.yd.tm.constant.TaskTestData.USER_1_PROJECT_1_TASK_DTO_1;
import static com.t1.yd.tm.constant.UserTestData.ALL_USER_DTOS;
import static com.t1.yd.tm.constant.UserTestData.USER_DTO_1;

;

@Tag("com.t1.yd.tm.marker.UnitCategory")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class ProjectDTOTaskDTOServiceTest {

    @Autowired
    private IProjectDtoService projectDtoService;

    @Autowired
    private ITaskDtoService taskDtoService;

    @Autowired
    private IDtoProjectTaskService service;

    @Autowired
    private IUserDtoService userDtoService;

    @BeforeEach
    public void initRepository() {
        taskDtoService.clear();
        projectDtoService.clear();
        userDtoService.clear();

        userDtoService.add(ALL_USER_DTOS);
    }

    @Test
    public void bindTaskToProject() {
        projectDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        @NotNull final TaskDTO bindedTaskDTO = taskDtoService.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId());
        Assertions.assertEquals(USER_1_PROJECT_DTO_1.getId(), bindedTaskDTO.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        service.unbindTaskFromProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        Assertions.assertTrue(taskDtoService.findAllByProjectId(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).isEmpty());
    }

    @Test
    public void removeProjectById() {
        projectDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        taskDtoService.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.bindTaskToProject(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        service.removeProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId());
        Assertions.assertTrue(taskDtoService.findAllByProjectId(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).isEmpty());
    }

    @AfterEach
    public void clearData() {
        taskDtoService.clear();
        projectDtoService.clear();
        userDtoService.clear();
    }

}